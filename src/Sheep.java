
public class Sheep {

   enum Animal {sheep, goat}

   public static void main(String[] param) {
      // for debugging
      Animal[] animals = new Sheep.Animal[100];
      int i = 0;
      while (i < animals.length) {
         if (Math.random() <= 0.5) {
            animals[i] = Animal.sheep;
         } else {
            animals[i] = Animal.goat;
         }
         i++;
      }
      {
         int q = 0;
         while (q < animals.length) {
            System.out.print(animals[q] + ", ");
            q++;
         }
         Sheep.reorder(animals);
         System.out.print("REORDED:");
         int w = 0;
         while (w < animals.length) {
            System.out.print(animals[w] + ", ");
            w++;
         }
      }
   }

   public static void reorder(Animal[] animals) {
      // TODO!!! Your program here
      int n = animals.length;
      int j = 0;
      int k = n - 1;
      for (int i = 0; i < n; i++) {
         if (animals[i] == Animal.goat) {
            Sheep.Animal temp = animals[i];
            animals[i] = animals[j];
            animals[j] = temp;
            j++;
         }
      }
      for (int l = n - 1; l > 0; l--) {
         if (animals[l] == Animal.sheep) {
            Sheep.Animal temp = animals[l];
            animals[l] = animals[k];
            animals[k] = temp;
            k--;
         }
      }
      System.out.println();
   }
}

